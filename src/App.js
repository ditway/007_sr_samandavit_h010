import React from "react";
import Header from "./components/Header";
import { Container } from "react-bootstrap";
import FormAuthor from "./components/FormAuthor";
import DisplayData from "./components/DisplayData";
import Provider from "./Provider";

import "./App.css";
import Loading from "./components/Loading";

export default function () {
  return (
    <Provider>
      <Header />
      <Container>
        <FormAuthor />
        <DisplayData />
        <Loading />
      </Container>
    </Provider>
  );
}
