import { Modal, Spinner } from "react-bootstrap";
import { useProvider } from "../Provider";

export default function () {
  const { loading, message } = useProvider();
  return (
    <Modal show={loading} centered backdrop="static">
      <Modal.Body>
       <div className='d-flex flex-column justify-content-center align-items-center'>
       <p>{message}</p>
       <Spinner animation='border'/>
       </div>
      </Modal.Body>
    </Modal>
  );
}
